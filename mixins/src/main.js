import Vue from 'vue'
import App from './App.vue'
// Global filter
Vue.filter('to-lowercase', function (value) {
  return value.toLowerCase();
});

// Global mixin is apply to all components. Please notice it.
Vue.mixin({
  created() {
    console.log('This is global mixin component');
  }
});
new Vue({
  el: '#app',
  render: h => h(App)
});

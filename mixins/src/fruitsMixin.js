export const fruitMixin =  {
    data() {
        return {
            text: 'Hello world!!',
            fruits: ['Banana', 'Apple', 'Mango', 'Melon'],
            filterText: ''
        }
    },
    filters: {
        toUppercase(value){
            return value.toUpperCase();
        }
    },
    computed: {
        filteredFruits() {
            return this.fruits.filter((element) => {
                return element.match(this.filterText);
            });
        }
    }
};
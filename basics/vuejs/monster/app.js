new Vue({
    el: '#app',
    data: {
        playerHealth: 100,
        monsterHealth: 100,
        gameIsRunning: false,
        turns: []
    },
    methods: {
        startNewGame() {
            this.gameIsRunning = !this.gameIsRunning;
            this.playerHealth = 100;
            this.monsterHealth = 100;
        },
        attack() {
            var damage = this.calculateDamage(3,10);
            this.monsterHealth -= damage;
            var message = 'Player hits for ' + damage;
            this.writeLog(message);
            if (this.checkWin()) {
                return;
            }

            this.monsterAttacks(5,12);
        },
        specialAttack(){
            this.monsterHealth -= this.calculateDamage(10,20);
            if (this.checkWin()) {
                return;
            }

            this.monsterAttacks(5,12);
        },
        monsterAttacks(min, max){
            var damage = this.calculateDamage(min, max);
            this.playerHealth -= damage;
            var message = 'Monster hits for ' + damage;
            this.writeLog(message);
            this.checkWin();
        },
        heal(){
            if (this.playerHealth <= 90) {
                this.playerHealth += 10;
            } else {
                this.playerHealth = 100;
            }
            var message = 'User heals for 10';
            this.writeLog(message);
            this.monsterAttacks(5,12);
        },
        giveUp(){
            this.gameIsRunning = false;
        },
        calculateDamage(min, max) {
            return Math.max(Math.floor(Math.random() * max) + 1, min);
        },
        checkWin() {
            if (this.monsterHealth < 0) {
                if (confirm('You won! New Game?')) {
                    this.startNewGame();
                    return;
                }
                this.gameIsRunning = false;
                return true;
            } else if (this.playerHealth <= 0) {
                if (confirm('You lost! New Game?')) {
                    this.startNewGame();
                    return true;
                }

                this.gameIsRunning = false;
            }

            return false;
        },
        writeLog(message) {
            this.turns.unshift({
                isPlayer: false,
                text: message
            });
        }
    }
});